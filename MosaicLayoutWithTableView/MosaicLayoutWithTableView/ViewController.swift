//
//  ViewController.swift
//  MosaicLayoutWithTableView
//
//  Created by Amit Sahu on 07/11/17.
//  Copyright © 2017 Amit Sahu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var data: [String] = ["Sunday Sunday MON","Monday Tuesday","Tuesday","Wednesday Monday FRI","Thursday","Friday Friday","Saturday","Sunday Sunday MON","Monday Tuesday","Sunday Sunday MON","Monday Tuesday"]
    
   var dataSet = [["Sunday Sunday MON ","Monday Tuesday","Tuesday","Wednesday Monday FRI","Thursday","Friday Friday","Saturday","Sunday Sunday MON","Monday Tuesday","Sunday Sunday MON","Monday Tuesday"],
    ["Sunday Sunday MON","Monday Tuesday","Tuesday","Wednesday Monday FRI","Thursday","Sunday"],
    ["Sunday Sunday MON","Monday Tuesday","Tuesday","Wednesday Monday FRI","Thursday"],
    ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSet.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath) as! TableViewCell
        
        cell.setCollectionViewDataSourceDelegate(row: indexPath.section)
        cell.updateData(dataSet[indexPath.section])
        //cell.collectionViewDelegate = self
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let totalItem: CGFloat = CGFloat(self.dataSet[indexPath.section].count)
        let itemHeight = Constant.itemHeight
        
        //let totalRow = ceil(Constant.totalItem / Constant.column)
        
        var width: CGFloat = 0.0
        for text in dataSet[indexPath.section] {
            
            width += text.width(withConstraintedHeight: 14, font: .systemFont(ofSize: 17)) + Constant.cellPadding
        }
        let final = width + (Constant.offset * 2) + ((totalItem - 1) * Constant.minItemSpacing)
        //print(final)
        let totalRow = ceil(final / tableView.bounds.size.width)
        
        //print("width \(width) row: \(totalRow)")
        
        //let totalRow = ceil(totalItem / Constant.column)
        
        let totalTopBottomOffset = Constant.offset + Constant.offset
        
        let totalSpacing = CGFloat(totalRow - 1) * Constant.minLineSpacing
        
        let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
        
        
        return totalHeight
    }
}



