//
//  Helper.swift
//  MosaicLayoutWithTableView
//
//  Created by Amit Sahu on 07/11/17.
//  Copyright © 2017 Amit Sahu. All rights reserved.
//

import Foundation
import UIKit

class Constant {
    
    
    static let column: CGFloat = 5
    static let itemHeight: CGFloat = 60
    static let cellPadding: CGFloat = 14
    
    static let minLineSpacing: CGFloat = 10.0
    static let minItemSpacing: CGFloat = 10.0
    
    static let offset: CGFloat = 10.0 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
}

extension UICollectionViewCell {
    
    func addCornerRadius(_ amount: CGFloat){
        self.layer.cornerRadius = amount
        
    }
    
 
    func addBorder(_ width: CGFloat, color: UIColor){
        let borderView = CAShapeLayer()
        borderView.strokeColor = color.cgColor
        borderView.lineDashPattern = [6, 3]
        borderView.frame = CGRect(x: 0, y: 0, width: (self.layer.frame.width - 1) , height: (self.layer.frame.height - 2))
        borderView.fillColor = nil
        borderView.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(borderView)
        
    }
    
    func addDashed(){
        
        let color = UIColor.lightGray.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 1, y: 1, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: 0, y: 0)
        shapeLayer.fillColor = UIColor.black.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2.0
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [9,6]
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func addDashedLine() {
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [6, 6]
        yourViewBorder.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)//self.frame
        yourViewBorder.fillColor = UIColor.black.cgColor
        yourViewBorder.lineJoin = kCALineJoinRound
        print("widht: \(frame.width) height: \(frame.height)")
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), cornerRadius: 6).cgPath
        self.clipsToBounds = true
        
        self.layer.addSublayer(yourViewBorder)
    }
    
    func cellBorder(){
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
    

}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        if ceil(boundingBox.width) >= UIScreen.main.bounds.width{
            
            return UIScreen.main.bounds.width - (Constant.offset * 2) - Constant.cellPadding
            
        }
        
        //print("boxwidth: \(ceil(boundingBox.width)) device: \(UIScreen.main.bounds.width)")
        return ceil(boundingBox.width)
    }
    
}

extension UIView {
    
    func addDashedLineView(frame: CGRect) {
        
        let border = CAShapeLayer()
        border.strokeColor = UIColor.blue.cgColor
        border.lineDashPattern = [6, 6]
        border.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)//self.frame
        border.fillColor = UIColor.clear.cgColor
        border.lineJoin = kCALineJoinRound
        //print("widht: \(frame.width) fraheight: \(UIScreen.main.bounds.width)")
        border.fillColor = nil
        border.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), cornerRadius: 6).cgPath
        self.clipsToBounds = true
        
        self.layer.addSublayer(border)
    }
}
