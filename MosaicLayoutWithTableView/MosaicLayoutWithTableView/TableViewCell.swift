//
//  TableViewCell.swift
//  MosaicLayoutWithTableView
//
//  Created by Amit Sahu on 07/11/17.
//  Copyright © 2017 Amit Sahu. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var collectionData: [String] = []
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        //self.collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsetsMake(
                Constant.offset,    // top
                Constant.offset,    // left
                Constant.offset,    // bottom
                Constant.offset     // right
            )
            
            
            
            layout.minimumInteritemSpacing = Constant.minItemSpacing
            layout.minimumLineSpacing = Constant.minLineSpacing
        }
        
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(row: Int){
        self.collectionView.tag = row
        self.collectionView.reloadData()
    }
    
    func updateData(_ data: [String]){
        
        self.collectionData = data
    }
    
   


}

extension TableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.headerLabel.text = self.collectionData[indexPath.row]
        cell.headerLabel.sizeToFit()
        
        
        
        let width = self.collectionData[indexPath.row].width(withConstraintedHeight: 14, font: .systemFont(ofSize: 17)) + Constant.cellPadding
        cell.contentView.addDashedLineView(frame: CGRect(x: 0, y: 0, width: width, height: Constant.itemHeight))
        //print("col: \(collectionView.bounds.width)")
        cell.contentView.setNeedsLayout()
        cell.contentView.layoutIfNeeded()
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let itemWidth = Constant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        let width = self.collectionData[indexPath.row].width(withConstraintedHeight:  14, font: .systemFont(ofSize: 17)) + Constant.cellPadding

        return CGSize(width: width , height: Constant.itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //print(indexPath.row)
        //print("index: \(indexPath.row) sec: \(indexPath.section)")
        
        
    }
   
    
}
